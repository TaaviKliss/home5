
import java.util.*;


public class Node {
	
	// Konstruktori koostamise põhimõte on siit võetud http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
	// ja ka siit http://stackoverflow.com/questions/3522454/java-tree-data-structure
   private String name;
   private Node firstChild;
   private Node nextSibling;
   private static LinkedList<String>soneElemendid;

   Node (String n, Node d, Node r) {
	   
	   	this.name = n;
	   	this.firstChild = d;
	   	this.nextSibling = r;
   }
   
   public static void soneKontroll(String kontrollitav) {
	   
	  int kumerSulg = 0;
	  int nogusSulg = 0;
	  int koma = 0;
	  soneElemendid = new LinkedList<String>(Arrays.asList(kontrollitav.split("")));
	  
	  // sõne elementide ükshaaval läbikäimine on inspireeritud siit https://www.dotnetperls.com/tree-java
	  for (String kontrollitavElement : soneElemendid) {
		  
		  if(kontrollitavElement.replaceAll("\\s+", " ").equals(" ")) {
			  
			  throw new RuntimeException("Sõne "
					  + "'" + kontrollitav + "'" + " on tühi (SONEKONTROLL).");
		  }
		  
		  if(kontrollitavElement.equals(",")) {
			  
			  koma++;
		  }
		  
		  if(kontrollitavElement.equals("(")) {
			  
			  kumerSulg++;
		  }
		  // Juurde lisatud
		  if(koma < ((kumerSulg + nogusSulg) / 2)) {
			  
			  throw new RuntimeException("Sõnes "
					  + "'" + kontrollitav + "'" + " on komasid puudu (SONEKONTROLL).");
		  }
		  // Juurde lisatud
		  else if(kontrollitavElement.equals(")")) {
			  
			  nogusSulg++;
			  
			  if(kumerSulg < 0 || nogusSulg < 0){
				  
				  throw new RuntimeException("Sõnes "
					  + "'" + kontrollitav + "'" + " on sulgi liialt (SONEKONTROLL).");
			  }
		  }
		  														// || märk on muudetud &&
		  else if(kontrollitavElement.equals(",") && (kumerSulg == 0 && nogusSulg == 0)) {
			  
			  throw new RuntimeException("Sõnel "
					  + "'" + kontrollitav + "'" + " puuduvad sulud (SONEKONTROLL).");
		  }
	    }
   }
   
   public static Node parsePostfix (String s) {
	   
      soneKontroll(s);      
      Node juur = struktuur(new Node(null,null,null),soneElemendid);
      
      if(juur.firstChild == null && juur.nextSibling == null) {
    	  
    	  if(juur.name != null) {
    		  
    		  return juur;
    	  }
      }
      
      else if(juur.firstChild == null && juur.nextSibling !=null) {
    	  
    	  throw new RuntimeException("Sõnel "
				  + "'" + s + "'" + " puuduvad sulud (PARSEPOSTFIX).");
      }
      return juur;
   }
   
   // createTree meetod on eeskujuks võetud http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
   public static Node moodustaPuu (Node juur, int avaSulg) {
	   
		if (juur.firstChild != null) {
			
			avaSulg++;
			juur.name += "(" + moodustaPuu(juur.firstChild, avaSulg).name;
			juur.name += ")";
			
			if (juur.nextSibling != null) {
				
				juur.name += "," + moodustaPuu(juur.nextSibling, avaSulg).name;
				return juur;
			}
		}
		
		else if (juur.nextSibling != null) {
			
			juur.name += "," + moodustaPuu(juur.nextSibling, avaSulg).name;
			return juur;
		} else {
			return juur;
		}
		return juur;
	}

   public String leftParentheticRepresentation() {
	   
	   String text = moodustaPuu(this, 0).name;
	   return text;
   }
   
   public static Node struktuur (Node juur, LinkedList<String>soneElemendid) {
	   
	   // LinkedList'i kasutamine meetodis ja koos while tsükliga https://www.cs.cmu.edu/~adamchik/15-121/lectures/Linked%20Lists/linked%20lists.html
	   while(!soneElemendid.isEmpty()) {
		   
		   String soneElement = soneElemendid.pop();
		   
		   if(soneElement.equals("(")) {
			   
			   juur.firstChild = struktuur(new Node(null,null,null),soneElemendid);
		   }
		   
		   else if(soneElement.equals(",")) {
			   
			   juur.nextSibling = struktuur(new Node(null,null,null),soneElemendid);
			   
			   if(juur.name == null) {
				   
				   throw new RuntimeException("Enne märki "
						   + "'" + soneElement + "'" + " on vajalik element puudu (STRUKTUUR).");
			   }
			   return juur;
		   }
		   
		   else if(soneElement.equals(")")) {
			   
			   if(juur.name == null) {
				   
				   throw new RuntimeException("Enne märki "
						   + "'" + soneElement + "'" + " on vajalik element puudu (STRUKTUUR).");
			   }
			   return juur;
		   } else {
			   if(juur.name == null)
			   {
				   
				   juur.name = soneElement;
			   } else {
				   juur.name += soneElement;
			   }
		   }
	   }
	   
	   if(juur.name == null) {
		   
		   throw new RuntimeException("Väljaspool sulge olev element on puudu (STRUKTUUR).");
	   }
	return juur;
   }

   public static void main (String[] param) {
	   
      String s = "(B1,(hh,)jj,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

}